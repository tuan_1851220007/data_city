
import requests
import pymysql
# jsson chuyển file lấy từ web về đọc dữ liệu
import json
import csv
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
import time

file="./daotukhoa.csv"
file2="./danhsachtukhoa.csv"
x = ""
# Nhap tu khoa can dao
print("Nhập từ khóa cần đào:")
x = input()

key_url = "https://shopee.vn/api/v4/search/search_hint?keyword={} &search_type=0&version=1".format(x)
def crawl_key():
    key_list = np.array([]);
    headers = {
          'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"}
    response = requests.request("GET",key_url)
    data= response.json();
    for item in data["keywords"]:
      
        key = (item["keyword"])
        key_list = np.append(key_list,[key],axis=0)
    key_list = key_list.astype(str)
    return key_list   
def write_csv_file(data_matrix,file_path,mode='w+'):
 	df = pd.DataFrame(data=data_matrix)
 	# coluoms userid itemid rating timestamp comment
 	df.to_csv(file_path,sep='\t',encoding='utf-8',header=False,index=False, mode=mode)
# cho vào list de tí đảo 2 lần

def crawl_keys(key_list):
    counts = -1;
    key_lists = np.array([]);
    for key in  key_list:
        key_urls = "https://shopee.vn/api/v4/search/search_hint?keyword={} &search_type=0&version=1".format(key)
        headers = {
          'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"}
        responses = requests.request("GET",key_urls)
        datas= responses.json();
        for items  in datas["keywords"]:
            keys =(items["keyword"])
            if(keys != items):
                key_lists = np.append(key_lists,[keys],axis=0)
        key_lists = key_lists.astype(str)


        # dao làn 3

    for key in key_lists:
        key_urls = "https://shopee.vn/api/v4/search/search_hint?keyword={} &search_type=0&version=1".format(key)
        headers = {
          'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"}
        responses = requests.request("GET",key_urls)
        datas= responses.json();
        for items  in datas["keywords"]:
            keys =(items["keyword"])
            if(keys != items):
                print(keys) 
                key_lists = np.append(key_lists,[keys],axis=0)
        key_lists = key_lists.astype(str)
    
    return key_lists

def crawl_count_key(key_lists):
    key_lists_count = np.array([['Key_words','Lượt tìm kiếm']]);
    for key in key_lists:
        key_url_count ="https://shopee.vn/api/v4/search/search_items?by=relevancy&keyword={}&limit=60&newest=0&order=desc&page_type=search&scenario=PAGE_GLOBAL_SEARCH&version=2".format(key)
        headers = {
            'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"}
        responses = requests.request("GET",key_url_count)
        data= responses.json();
        counts = data["query_rewrite"]["ori_total_count"]
        print(key)
        print(counts)
        key_lists_count = np.append(key_lists_count,[[key,counts]],axis=0)
      
    return key_lists_count


key_list = crawl_key()
key_lists = crawl_keys(key_list)
write_csv_file(key_lists,file2,mode='w+')
#  bỏ đi các từ khóa trùng lặp
s = set(key_lists) 
data_list = crawl_count_key(s)
write_csv_file(data_list,file,mode ='w+')