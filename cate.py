
import requests
import pymysql
# jsson chuyển file lấy từ web về đọc dữ liệu
import json
import csv
import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
import time
file2="./category.csv"

id= -1;
display_name= -1;
urls = "https://shopee.vn/api/v4/pages/get_category_tree";

def crawl_category():
    category_list = np.array([]);
    headers = {
          'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"}
    response = requests.request("GET","https://shopee.vn/api/v4/pages/get_category_tree")
    data= response.json();

    # taoj 1 mang danh sach chua  id 
    
    for item in data["data"]['category_list']:
        id = item["catid"]
        category_list=np.append(category_list,[id],axis=0)
        display_name = item["display_name"]

       
        #print(display_name)
    category_list  =  category_list.astype(int)
    #print(category_list)
    return category_list


def write_csv_file(data_matrix,file_path,mode='w'):
 	df = pd.DataFrame(data=data_matrix)
 	# coluoms userid itemid rating timestamp comment
 	df.to_csv(file_path,sep='\t',encoding='utf-8',header=False,index=False, mode=mode)

def read_matrix_file(file_path):
 	f = pd.read_csv(
 			file_path, sep='\t',encoding='utf-8',headers=None)
 	f = f.to_numpy()
 	return f


def crawl_category_list(category_list):
    y = 0;
    lenx =0;
    category_detail_list = np.array([["Category_Main","parent_catid","Name"]])
    
    headers = {
          'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"}
    response = requests.request("GET","https://shopee.vn/api/v4/pages/get_category_tree")
    data= response.json();
    #duyet lisst
    Category_Main = -1
    parent_catid = -1;
    Name = -1;
    for item in data["data"]['category_list']:
        Category_Main = item["display_name"] 
        lenx =  len(item["children"]);
        try:
            while(y <= lenx):
                parent_catid = (item["children"][y]["parent_catid"])
                Name =  (item["children"][y]["display_name"])
                y = y+1
                print(Name)
                category_detail_list = np.append(category_detail_list,[[Category_Main,parent_catid,Name]],axis=0)
        except:
            y = 0;    
      
    return category_detail_list
category_list = crawl_category(); 
category_detail_list = crawl_category_list(category_list)
write_csv_file(category_detail_list,file2,mode ='w+')


